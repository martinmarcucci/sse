// Librerias Estandares de C
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Librerias Especificas del Sistema Operativo
#include "ch.h"
#include "hal.h"

// Nuestras tareas
#include "tareas.h"

// Reservo area de memoria para las tareas
WORKING_AREA(waLED1, 128);
WORKING_AREA(waLED2, 128);
WORKING_AREA(waLED3, 128);
WORKING_AREA(waLED4, 128);
WORKING_AREA(waCOMBO, 128);
WORKING_AREA(waSerie, 128);
WORKING_AREA(waHora, 128);

// Reservo lugar para los mailbox
WORKING_AREA(Msg1, 128);
MAILBOX_DECL(mb1, Msg1, MB_SIZE);
WORKING_AREA(Msg2, 128);
MAILBOX_DECL(mb2, Msg2, MB_SIZE);
WORKING_AREA(Msg3, 128);
MAILBOX_DECL(mb3, Msg3, MB_SIZE);
WORKING_AREA(Msg4, 128);
MAILBOX_DECL(mb4, Msg4, MB_SIZE);
WORKING_AREA(Msg5, 128);
MAILBOX_DECL(mb5, Msg5, MB_SIZE);

// Creo Semaforo
SEMAPHORE_DECL(uso_led, 0);

// Funciones de ayuda para utilizar el puerto serie
void writeLn(const char * string);
void apagar_leds();
int get_num(char c);

/*
 * Tarea que es creda cuatro veces, una para cada uno de los leds.
 *
 * Tiene como argumento una estructura del tipo tled, que informa
 * sobre el led que debe manejar y el mailbox al que debe escuchar.
 */
msg_t TareaLED(void *arg) {

	tled info_t;
	int time = 0; // Tiempo para toglear el led
	info_t = *(tled*) arg;

	msg_t msg, ret;

	chRegSetThreadName("Blinker");

	//loop principal de la tarea
	while (TRUE) {
		// Si es el primer inicio o hay algun mensaje de apagado
		if (time == 0) {
			palClearPad(GPIOD, info_t.led);

			chSemSignal(&uso_led); // devuelvo el uso del semaforo
			chThdSleep(TIME_INFINITE); // me voy a dormir
			chSemWait(&uso_led); // cuando despierto pido el semaforo de nuevo
			time = 10; // cambio el 0 para no dormir de nuevo
		}
		chThdSleepMilliseconds( time);
		// delay entre toggles
		palTogglePad(GPIOD, info_t.led);

		// Veo si hay mensajes en el mailbox
		ret = chMBFetch(info_t.mb, &msg, TIME_IMMEDIATE);
		if (ret == RDY_OK) {
			time = (int) msg; // si llego el mailbox, grabo el dato en el tiempo
		}
	}
	return 0;
}

/*
 * Tarea encargada de mostra la hora por la terminal. Se inicia enviandole
 * un mailbox con una hora valida, y se para enviandole una tarea invalida.
 */
msg_t TareaHora(void *arg) {
	msg_t msg, ret;
	char *hora_num;
	char hora = -1, min = -1, seg = -1;
	chRegSetThreadName("Hora");
	while (TRUE) {
		// Verificio si la hora es valida
		if (hora >= 0 && hora <= 23 && min >= 0 && min <= 59 && seg >= 0
				&& seg <= 59) {
			// si es valida, incremento y recalculo valores
			seg++;
			if (seg == 60) {
				seg = 0;
				min++;
				if (min == 60) {
					min = 0;
					hora++;
					if (hora == 24)
						hora = 0;
				}
			}
		} else {
			// Si era invalida, espero a que me manden un nuevo mensaje
			ret = chMBFetch(&mb5, &msg, TIME_INFINITE);
		}
		// Si hay mensaje, gravo los valores en las variables
		if (ret == RDY_OK) {
			hora_num = (char *) msg;

			hora = hora_num[0] * 10 + hora_num[1];
			min = hora_num[2] * 10 + hora_num[3];
			seg = hora_num[4] * 10 + hora_num[5];
		}
		chThdSleepMilliseconds(1000);

		// cada 10 segundos muestro la hora
		if (seg % 10 == 0) {
			write("Hora:  ");
			sdPut(&SD2, (hora/10) + 48);
			sdPut(&SD2, hora - ((hora/10)*10) + 48);
			sdPut(&SD2, ':');
			sdPut(&SD2, (min/10) + 48);
			sdPut(&SD2, min - ((min/10)*10) + 48);
			sdPut(&SD2, ':');
			sdPut(&SD2, (seg/10) + 48);
			sdPut(&SD2, seg - ((seg/10)*10) + 48);
			writeLn("");
		}
		// Evaluo el mailbox
		ret = chMBFetch(&mb5, &msg, TIME_IMMEDIATE);
	}
	return 0;
}

/*
 *  Tarea que hace un circulo con los leds, esta toma los 4 leds con el
 *  counting semafore.
 */
msg_t TareaCombo(void *arg) {
	char vuelta = 0;

	chRegSetThreadName("combo");

	chThdSleep(TIME_INFINITE);

	chSemWait(&uso_led);
	chThdSleepMilliseconds( 1);
	chSemWait(&uso_led);
	chThdSleepMilliseconds( 1);
	chSemWait(&uso_led);
	chThdSleepMilliseconds( 1);
	chSemWait(&uso_led);

	while (TRUE) {
		if (vuelta > 6) {
			palClearPad(GPIOD, GPIOD_LED5);

			chSemSignal(&uso_led);
			chSemSignal(&uso_led);
			chSemSignal(&uso_led);
			chSemSignal(&uso_led);

			chThdSleep(TIME_INFINITE);

			chSemWait(&uso_led);
			chSemWait(&uso_led);
			chSemWait(&uso_led);
			chSemWait(&uso_led);

			vuelta = 0;
		} else {
			vuelta++;
		}
		apagar_leds();
		palSetPad(GPIOD, GPIOD_LED3);
		chThdSleepMilliseconds( 100);

		apagar_leds();
		palSetPad(GPIOD, GPIOD_LED4);
		chThdSleepMilliseconds( 100);

		apagar_leds();
		palSetPad(GPIOD, GPIOD_LED6);
		chThdSleepMilliseconds( 100);

		apagar_leds();
		palSetPad(GPIOD, GPIOD_LED5);
		chThdSleepMilliseconds( 100);
	}
	return 0;
}

/*
 *  Tarea dedicada a procesar comandos del puerto serie y
 *  resumir y enviar mensaes a las otras tareas.
 */
msg_t Serie(void *arg) {
	char cont = 0, i, error = 0, tmp;
	char hora[6];
	char buffer[64];
	int dato;

	chRegSetThreadName("RX puerto serie");
	while (TRUE) {
		// Traigo un caracter de la cola
		buffer[cont] = sdGet(&SD2);

		// Si el caracter es un 10 (CR) proceso el comando
		if (buffer[cont] == 10) {
			// Fuerzo a un string terminado con 0
			buffer[cont] = 0x00;
			error = 0;

			// escribo el comando en pantalla
			writeLn(buffer);
			// Verifico que el comando comience con led
			if ((buffer[0] == 'L' || buffer[0] == 'l')
					&& (buffer[1] == 'E' || buffer[1] == 'e')
					&& (buffer[2] == 'D' || buffer[2] == 'd')
					&& (buffer[4] == ' ')) {

				// Verifico el numero y lo almaceno como dato recibido
				dato = 0;
				for (i = 5; i < cont; i++) {
					tmp = get_num(buffer[i]);
					if (tmp == -1) {
						error++;
					} else {
						dato *= 10;
						dato += tmp;
					}
				}

				// Si no hubo error en el numero, analizo el comando
				if (error == 0) {
					switch (buffer[3]) {
					case 'n':
					case 'N':
						chThdResume(t_led1);
						chMBReset(&mb1);
						chMBPost(&mb1, dato, TIME_IMMEDIATE);
						break;
					case 'v':
					case 'V':
						chThdResume(t_led2);
						chMBReset(&mb2);
						chMBPost(&mb2, dato, TIME_IMMEDIATE);
						break;
					case 'r':
					case 'R':
						chThdResume(t_led3);
						chMBReset(&mb3);
						chMBPost(&mb3, dato, TIME_IMMEDIATE);
						break;
					case 'a':
					case 'A':
						chThdResume(t_led4);
						chMBReset(&mb4);
						chMBPost(&mb4, dato, TIME_IMMEDIATE);
						break;
					case 'c':
					case 'C':
						chMBPost(&mb1, 0, TIME_IMMEDIATE);
						chMBPost(&mb2, 0, TIME_IMMEDIATE);
						chMBPost(&mb3, 0, TIME_IMMEDIATE);
						chMBPost(&mb4, 0, TIME_IMMEDIATE);
						chThdResume(t_combo);
						break;
					default:
						error = 1;
					}
				}
			} else if ((buffer[0] == 'T' || buffer[0] == 't')
					&& (buffer[1] == 'I' || buffer[1] == 'i')
					&& (buffer[2] == 'M' || buffer[2] == 'm')
					&& (buffer[3] == 'E' || buffer[3] == 'e')
					&& (buffer[4] == ' ')) {
				//Verifico si la hora es valida
				for (i = 0; i < 6; i++) {
					hora[i] = get_num(buffer[i + 5]);
					if (hora[i] == -1)
						error++;
				}
				if (error == 0) {
					// Si no hubo error, envio el mensaje con la hora
					chMBPost(&mb5, hora, TIME_IMMEDIATE);
				}
			} else if ((buffer[0] == 'A' || buffer[0] == 'a')
					&& (buffer[1] == 'Y' || buffer[1] == 'y')
					&& (buffer[2] == 'U' || buffer[2] == 'u')
					&& (buffer[3] == 'D' || buffer[3] == 'd')
					&& (buffer[4] == 'A' || buffer[4] == 'a')) {
				writeLn("");
				writeLn("");
				writeLn("");
				writeLn("");
				writeLn("AYUDA");
				writeLn("-----");
				writeLn("");
				writeLn(
						"ledX yyyy :    Titila el led X (a/v/n/r) con un periodo yyyy");
				writeLn("ledc x:         Realiza el combo de leds (x puede ser cualquier numero)");
				writeLn(
						"time hhmmss  : Configura la hora del micro y habilita el informar");
				writeLn("               la hora cada 10 segundos");

			} else {
				error++;
			}

			// si algun comando dio error, informo que el comando  no es correcto
			if (error) {
				writeLn("Comando incorrecto");
			}
			cont = 0;

		} else if (buffer[cont] != 13) {
			cont++;
		}
	}
	return 0;
}

// Funcion para escribir un string
void write(const char * string) {
	while (*string) {
		sdPut( &SD2, *( string++ ));
	}
}
// Escribe un string y lo finaliza con enter
void writeLn(const char * string) {
	write(string);
	sdWrite(&SD2, (const u8 *) "\r\n", 2);
}

// Apago todos los leds
void apagar_leds() {
	palClearPad(GPIOD, GPIOD_LED3);
	palClearPad(GPIOD, GPIOD_LED4);
	palClearPad(GPIOD, GPIOD_LED5);
	palClearPad(GPIOD, GPIOD_LED6);
}

// Verifica si un numero en assci es valido y lo retorna como int.
int get_num(char c) {
	if (c < '0' || c > '9') {
		return -1;
	}
	return (c - 48);
}

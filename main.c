// Librerias Estandares de C
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Librerias Especificas del Sistema Operativo
#include "ch.h"
#include "hal.h"

// Nuestras tareas
#include "tareas.h"

int main(void) {
	// Descripcion de cada tarea Led
	tled led1, led2, led3, led4;

	// Inicializamod el Hal y el Sistema operativo
	halInit();
	chSysInit();

	// Inicializamos el puerto serie
	sdStart(&SD2, NULL);
	palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7)); //TX
	palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7)); //RX

	// Inicializamos los mailbox que vamos a usar
	chMBInit(&mb1, (msg_t *) Msg1, MB_SIZE);
	chMBInit(&mb2, (msg_t *) Msg2, MB_SIZE);
	chMBInit(&mb3, (msg_t *) Msg3, MB_SIZE);
	chMBInit(&mb4, (msg_t *) Msg4, MB_SIZE);
	chMBInit(&mb5, (msg_t *) Msg4, MB_SIZE);

	// Escribo una pquenia cabecera.
	writeLn("");
	writeLn("");
	writeLn("");
	writeLn("");
	writeLn("Programa Iniciado");
	writeLn("");

	/*
	 * Creamos todas las tereas de led
	 */
	led1.led = GPIOD_LED3;
	led1.mb = &mb1;
	t_led1 = chThdCreateStatic(waLED1, sizeof(waLED1), NORMALPRIO, TareaLED,
			(void*) &led1);
	led2.led = GPIOD_LED4;
	led2.mb = &mb2;
	t_led2 = chThdCreateStatic(waLED2, sizeof(waLED2), NORMALPRIO, TareaLED,
			(void*) &led2);
	led3.led = GPIOD_LED5;
	led3.mb = &mb3;
	t_led3 = chThdCreateStatic(waLED3, sizeof(waLED3), NORMALPRIO, TareaLED,
			(void*) &led3);
	led4.led = GPIOD_LED6;
	led4.mb = &mb4;
	t_led4 = chThdCreateStatic(waLED4, sizeof(waLED4), NORMALPRIO, TareaLED,
			(void*) &led4);

	// Inicializamos la tarea Combo
	t_combo = chThdCreateStatic(waCOMBO, sizeof(waCOMBO), NORMALPRIO,
			TareaCombo, NULL);

	// Iniciamos las tareas de puerto serie y de la hora.
	chThdCreateStatic(waHora, sizeof(waHora), NORMALPRIO + 1, TareaHora, NULL);
	chThdCreateStatic(waSerie, sizeof(waSerie), NORMALPRIO + 2, Serie, NULL);


	// el loop del main puede o no se usado como una tarea. (no lo usamos)
	while (1) {

	}
}


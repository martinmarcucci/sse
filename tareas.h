#ifndef _TAREAS_H_
#define _TAREAS_H_

// Exporto los working harea para que esten disponibles
extern WORKING_AREA(waLED1, 128);
extern WORKING_AREA(waLED2, 128);
extern WORKING_AREA(waLED3, 128);
extern WORKING_AREA(waLED4, 128);
extern WORKING_AREA(waCOMBO, 128);
extern WORKING_AREA(waSerie, 128);
extern WORKING_AREA(waHora, 128);

// creo los punteros para las tareas que deben ser resumidas
Thread *t_led1, *t_led2, *t_led3, *t_led4, *t_combo;

// exporto el semarofo
extern Semaphore uso_led;

//exporto los mailboxes
#define MB_SIZE 5
extern WORKING_AREA(Msg1, 128);
extern Mailbox mb1;
extern WORKING_AREA(Msg2, 128);
extern Mailbox mb2;
extern WORKING_AREA(Msg3, 128);
extern Mailbox mb3;
extern WORKING_AREA(Msg4, 128);
extern Mailbox mb4;
extern WORKING_AREA(Msg5, 128);
extern Mailbox mb5;

// defino la estructura con info para las tareas de leds
typedef struct {
	char led;
	Mailbox *mb;
} tled;

// prototipos de las tareas
msg_t TareaLED(void *arg);
msg_t Serie(void *arg);
msg_t TareaCombo(void *arg);
msg_t TareaHora(void *arg);

#endif
